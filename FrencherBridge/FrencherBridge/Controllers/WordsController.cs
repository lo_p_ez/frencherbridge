﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FrencherBridge.Models;

namespace FrencherBridge.Controllers
{
    public class WordsController : ApiController
    {
        Word[] words = new Word[]
        {
            new Word {Id=1, French="bonjour", Spanish="hola" },
            new Word {Id=2, French="au revoir", Spanish="adiós" },
            new Word {Id=3, French="à plus tard", Spanish="hasta luego" }
        };

        public IEnumerable<Word> GetAllWords()
        {
            return words;
        }

        public IHttpActionResult GetWord(int id)
        {
            var word = words.FirstOrDefault((p) => p.Id == id);
            if (word == null)
            {
                return NotFound();
            }
            return Ok(word);
        }
    }
}
