﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FrencherBridge.Models
{
    public class Word
    {
        public int Id { get; set; }
        public string Spanish { get; set; }
        public string French { get; set; }
    }
}